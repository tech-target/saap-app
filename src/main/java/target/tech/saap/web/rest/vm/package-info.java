/**
 * View Models used by Spring MVC REST controllers.
 */
package target.tech.saap.web.rest.vm;
