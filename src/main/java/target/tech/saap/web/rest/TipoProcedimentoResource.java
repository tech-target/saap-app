package target.tech.saap.web.rest;

import target.tech.saap.domain.TipoProcedimento;
import target.tech.saap.repository.TipoProcedimentoRepository;
import target.tech.saap.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link target.tech.saap.domain.TipoProcedimento}.
 */
@RestController
@RequestMapping("/api")
public class TipoProcedimentoResource {

    private final Logger log = LoggerFactory.getLogger(TipoProcedimentoResource.class);

    private static final String ENTITY_NAME = "tipoProcedimento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoProcedimentoRepository tipoProcedimentoRepository;

    public TipoProcedimentoResource(TipoProcedimentoRepository tipoProcedimentoRepository) {
        this.tipoProcedimentoRepository = tipoProcedimentoRepository;
    }

    /**
     * {@code POST  /tipo-procedimentos} : Create a new tipoProcedimento.
     *
     * @param tipoProcedimento the tipoProcedimento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoProcedimento, or with status {@code 400 (Bad Request)} if the tipoProcedimento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-procedimentos")
    public ResponseEntity<TipoProcedimento> createTipoProcedimento(@Valid @RequestBody TipoProcedimento tipoProcedimento) throws URISyntaxException {
        log.debug("REST request to save TipoProcedimento : {}", tipoProcedimento);
        if (tipoProcedimento.getId() != null) {
            throw new BadRequestAlertException("A new tipoProcedimento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoProcedimento result = tipoProcedimentoRepository.save(tipoProcedimento);
        return ResponseEntity.created(new URI("/api/tipo-procedimentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-procedimentos} : Updates an existing tipoProcedimento.
     *
     * @param tipoProcedimento the tipoProcedimento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoProcedimento,
     * or with status {@code 400 (Bad Request)} if the tipoProcedimento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoProcedimento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-procedimentos")
    public ResponseEntity<TipoProcedimento> updateTipoProcedimento(@Valid @RequestBody TipoProcedimento tipoProcedimento) throws URISyntaxException {
        log.debug("REST request to update TipoProcedimento : {}", tipoProcedimento);
        if (tipoProcedimento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoProcedimento result = tipoProcedimentoRepository.save(tipoProcedimento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tipoProcedimento.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipo-procedimentos} : get all the tipoProcedimentos.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoProcedimentos in body.
     */
    @GetMapping("/tipo-procedimentos")
    public List<TipoProcedimento> getAllTipoProcedimentos() {
        log.debug("REST request to get all TipoProcedimentos");
        return tipoProcedimentoRepository.findAll();
    }

    /**
     * {@code GET  /tipo-procedimentos/:id} : get the "id" tipoProcedimento.
     *
     * @param id the id of the tipoProcedimento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoProcedimento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-procedimentos/{id}")
    public ResponseEntity<TipoProcedimento> getTipoProcedimento(@PathVariable Long id) {
        log.debug("REST request to get TipoProcedimento : {}", id);
        Optional<TipoProcedimento> tipoProcedimento = tipoProcedimentoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tipoProcedimento);
    }

    /**
     * {@code DELETE  /tipo-procedimentos/:id} : delete the "id" tipoProcedimento.
     *
     * @param id the id of the tipoProcedimento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-procedimentos/{id}")
    public ResponseEntity<Void> deleteTipoProcedimento(@PathVariable Long id) {
        log.debug("REST request to delete TipoProcedimento : {}", id);
        tipoProcedimentoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
