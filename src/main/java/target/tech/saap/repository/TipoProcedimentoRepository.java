package target.tech.saap.repository;

import target.tech.saap.domain.TipoProcedimento;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoProcedimento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoProcedimentoRepository extends JpaRepository<TipoProcedimento, Long> {

}
