import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { TipoProcedimentoService } from './tipo-procedimento.service';
import { TipoProcedimentoComponent } from './tipo-procedimento.component';
import { TipoProcedimentoDetailComponent } from './tipo-procedimento-detail.component';
import { TipoProcedimentoUpdateComponent } from './tipo-procedimento-update.component';
import { TipoProcedimentoDeletePopupComponent } from './tipo-procedimento-delete-dialog.component';
import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

@Injectable({ providedIn: 'root' })
export class TipoProcedimentoResolve implements Resolve<ITipoProcedimento> {
  constructor(private service: TipoProcedimentoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITipoProcedimento> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TipoProcedimento>) => response.ok),
        map((tipoProcedimento: HttpResponse<TipoProcedimento>) => tipoProcedimento.body)
      );
    }
    return of(new TipoProcedimento());
  }
}

export const tipoProcedimentoRoute: Routes = [
  {
    path: '',
    component: TipoProcedimentoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TipoProcedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TipoProcedimentoDetailComponent,
    resolve: {
      tipoProcedimento: TipoProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TipoProcedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TipoProcedimentoUpdateComponent,
    resolve: {
      tipoProcedimento: TipoProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TipoProcedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TipoProcedimentoUpdateComponent,
    resolve: {
      tipoProcedimento: TipoProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TipoProcedimentos'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tipoProcedimentoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TipoProcedimentoDeletePopupComponent,
    resolve: {
      tipoProcedimento: TipoProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TipoProcedimentos'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
