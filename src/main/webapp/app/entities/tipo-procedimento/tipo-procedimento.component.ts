import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { AccountService } from 'app/core';
import { TipoProcedimentoService } from './tipo-procedimento.service';

@Component({
  selector: 'jhi-tipo-procedimento',
  templateUrl: './tipo-procedimento.component.html'
})
export class TipoProcedimentoComponent implements OnInit, OnDestroy {
  tipoProcedimentos: ITipoProcedimento[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected tipoProcedimentoService: TipoProcedimentoService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.tipoProcedimentoService
      .query()
      .pipe(
        filter((res: HttpResponse<ITipoProcedimento[]>) => res.ok),
        map((res: HttpResponse<ITipoProcedimento[]>) => res.body)
      )
      .subscribe(
        (res: ITipoProcedimento[]) => {
          this.tipoProcedimentos = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTipoProcedimentos();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITipoProcedimento) {
    return item.id;
  }

  registerChangeInTipoProcedimentos() {
    this.eventSubscriber = this.eventManager.subscribe('tipoProcedimentoListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
