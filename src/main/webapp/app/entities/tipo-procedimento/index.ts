export * from './tipo-procedimento.service';
export * from './tipo-procedimento-update.component';
export * from './tipo-procedimento-delete-dialog.component';
export * from './tipo-procedimento-detail.component';
export * from './tipo-procedimento.component';
export * from './tipo-procedimento.route';
