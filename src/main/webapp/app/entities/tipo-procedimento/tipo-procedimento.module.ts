import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SaapAppSharedModule } from 'app/shared';
import {
  TipoProcedimentoComponent,
  TipoProcedimentoDetailComponent,
  TipoProcedimentoUpdateComponent,
  TipoProcedimentoDeletePopupComponent,
  TipoProcedimentoDeleteDialogComponent,
  tipoProcedimentoRoute,
  tipoProcedimentoPopupRoute
} from './';

const ENTITY_STATES = [...tipoProcedimentoRoute, ...tipoProcedimentoPopupRoute];

@NgModule({
  imports: [SaapAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TipoProcedimentoComponent,
    TipoProcedimentoDetailComponent,
    TipoProcedimentoUpdateComponent,
    TipoProcedimentoDeleteDialogComponent,
    TipoProcedimentoDeletePopupComponent
  ],
  entryComponents: [
    TipoProcedimentoComponent,
    TipoProcedimentoUpdateComponent,
    TipoProcedimentoDeleteDialogComponent,
    TipoProcedimentoDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SaapAppTipoProcedimentoModule {}
