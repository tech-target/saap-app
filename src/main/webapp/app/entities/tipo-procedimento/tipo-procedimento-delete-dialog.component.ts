import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { TipoProcedimentoService } from './tipo-procedimento.service';

@Component({
  selector: 'jhi-tipo-procedimento-delete-dialog',
  templateUrl: './tipo-procedimento-delete-dialog.component.html'
})
export class TipoProcedimentoDeleteDialogComponent {
  tipoProcedimento: ITipoProcedimento;

  constructor(
    protected tipoProcedimentoService: TipoProcedimentoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.tipoProcedimentoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'tipoProcedimentoListModification',
        content: 'Deleted an tipoProcedimento'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-tipo-procedimento-delete-popup',
  template: ''
})
export class TipoProcedimentoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tipoProcedimento }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TipoProcedimentoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.tipoProcedimento = tipoProcedimento;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/tipo-procedimento', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/tipo-procedimento', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
