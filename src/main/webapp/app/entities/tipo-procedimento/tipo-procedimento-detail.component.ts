import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

@Component({
  selector: 'jhi-tipo-procedimento-detail',
  templateUrl: './tipo-procedimento-detail.component.html'
})
export class TipoProcedimentoDetailComponent implements OnInit {
  tipoProcedimento: ITipoProcedimento;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tipoProcedimento }) => {
      this.tipoProcedimento = tipoProcedimento;
    });
  }

  previousState() {
    window.history.back();
  }
}
