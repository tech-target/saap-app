import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

type EntityResponseType = HttpResponse<ITipoProcedimento>;
type EntityArrayResponseType = HttpResponse<ITipoProcedimento[]>;

@Injectable({ providedIn: 'root' })
export class TipoProcedimentoService {
  public resourceUrl = SERVER_API_URL + 'api/tipo-procedimentos';

  constructor(protected http: HttpClient) {}

  create(tipoProcedimento: ITipoProcedimento): Observable<EntityResponseType> {
    return this.http.post<ITipoProcedimento>(this.resourceUrl, tipoProcedimento, { observe: 'response' });
  }

  update(tipoProcedimento: ITipoProcedimento): Observable<EntityResponseType> {
    return this.http.put<ITipoProcedimento>(this.resourceUrl, tipoProcedimento, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITipoProcedimento>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITipoProcedimento[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
