import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITipoProcedimento, TipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { TipoProcedimentoService } from './tipo-procedimento.service';

@Component({
  selector: 'jhi-tipo-procedimento-update',
  templateUrl: './tipo-procedimento-update.component.html'
})
export class TipoProcedimentoUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    descricao: [null, [Validators.required]]
  });

  constructor(
    protected tipoProcedimentoService: TipoProcedimentoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tipoProcedimento }) => {
      this.updateForm(tipoProcedimento);
    });
  }

  updateForm(tipoProcedimento: ITipoProcedimento) {
    this.editForm.patchValue({
      id: tipoProcedimento.id,
      nome: tipoProcedimento.nome,
      descricao: tipoProcedimento.descricao
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tipoProcedimento = this.createFromForm();
    if (tipoProcedimento.id !== undefined) {
      this.subscribeToSaveResponse(this.tipoProcedimentoService.update(tipoProcedimento));
    } else {
      this.subscribeToSaveResponse(this.tipoProcedimentoService.create(tipoProcedimento));
    }
  }

  private createFromForm(): ITipoProcedimento {
    return {
      ...new TipoProcedimento(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      descricao: this.editForm.get(['descricao']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoProcedimento>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
