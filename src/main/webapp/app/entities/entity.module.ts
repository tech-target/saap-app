import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'aluno',
        loadChildren: () => import('./aluno/aluno.module').then(m => m.SaapAppAlunoModule)
      },
      {
        path: 'professor',
        loadChildren: () => import('./professor/professor.module').then(m => m.SaapAppProfessorModule)
      },
      {
        path: 'procedimento',
        loadChildren: () => import('./procedimento/procedimento.module').then(m => m.SaapAppProcedimentoModule)
      },
      {
        path: 'tipo-procedimento',
        loadChildren: () => import('./tipo-procedimento/tipo-procedimento.module').then(m => m.SaapAppTipoProcedimentoModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SaapAppEntityModule {}
