import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProcedimento } from 'app/shared/model/procedimento.model';

type EntityResponseType = HttpResponse<IProcedimento>;
type EntityArrayResponseType = HttpResponse<IProcedimento[]>;

@Injectable({ providedIn: 'root' })
export class ProcedimentoService {
  public resourceUrl = SERVER_API_URL + 'api/procedimentos';

  constructor(protected http: HttpClient) {}

  create(procedimento: IProcedimento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(procedimento);
    return this.http
      .post<IProcedimento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(procedimento: IProcedimento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(procedimento);
    return this.http
      .put<IProcedimento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IProcedimento>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IProcedimento[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(procedimento: IProcedimento): IProcedimento {
    const copy: IProcedimento = Object.assign({}, procedimento, {
      data: procedimento.data != null && procedimento.data.isValid() ? procedimento.data.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.data = res.body.data != null ? moment(res.body.data) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((procedimento: IProcedimento) => {
        procedimento.data = procedimento.data != null ? moment(procedimento.data) : null;
      });
    }
    return res;
  }
}
