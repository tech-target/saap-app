import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProcedimento } from 'app/shared/model/procedimento.model';
import { ProcedimentoService } from './procedimento.service';

@Component({
  selector: 'jhi-procedimento-delete-dialog',
  templateUrl: './procedimento-delete-dialog.component.html'
})
export class ProcedimentoDeleteDialogComponent {
  procedimento: IProcedimento;

  constructor(
    protected procedimentoService: ProcedimentoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.procedimentoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'procedimentoListModification',
        content: 'Deleted an procedimento'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-procedimento-delete-popup',
  template: ''
})
export class ProcedimentoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ procedimento }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ProcedimentoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.procedimento = procedimento;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/procedimento', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/procedimento', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
