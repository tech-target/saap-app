import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProcedimento } from 'app/shared/model/procedimento.model';

@Component({
  selector: 'jhi-procedimento-detail',
  templateUrl: './procedimento-detail.component.html'
})
export class ProcedimentoDetailComponent implements OnInit {
  procedimento: IProcedimento;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ procedimento }) => {
      this.procedimento = procedimento;
    });
  }

  previousState() {
    window.history.back();
  }
}
