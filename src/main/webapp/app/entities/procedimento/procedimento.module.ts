import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SaapAppSharedModule } from 'app/shared';
import {
  ProcedimentoComponent,
  ProcedimentoDetailComponent,
  ProcedimentoUpdateComponent,
  ProcedimentoDeletePopupComponent,
  ProcedimentoDeleteDialogComponent,
  procedimentoRoute,
  procedimentoPopupRoute
} from './';

const ENTITY_STATES = [...procedimentoRoute, ...procedimentoPopupRoute];

@NgModule({
  imports: [SaapAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ProcedimentoComponent,
    ProcedimentoDetailComponent,
    ProcedimentoUpdateComponent,
    ProcedimentoDeleteDialogComponent,
    ProcedimentoDeletePopupComponent
  ],
  entryComponents: [
    ProcedimentoComponent,
    ProcedimentoUpdateComponent,
    ProcedimentoDeleteDialogComponent,
    ProcedimentoDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SaapAppProcedimentoModule {}
