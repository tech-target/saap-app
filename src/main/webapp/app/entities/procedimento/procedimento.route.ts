import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Procedimento } from 'app/shared/model/procedimento.model';
import { ProcedimentoService } from './procedimento.service';
import { ProcedimentoComponent } from './procedimento.component';
import { ProcedimentoDetailComponent } from './procedimento-detail.component';
import { ProcedimentoUpdateComponent } from './procedimento-update.component';
import { ProcedimentoDeletePopupComponent } from './procedimento-delete-dialog.component';
import { IProcedimento } from 'app/shared/model/procedimento.model';

@Injectable({ providedIn: 'root' })
export class ProcedimentoResolve implements Resolve<IProcedimento> {
  constructor(private service: ProcedimentoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IProcedimento> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Procedimento>) => response.ok),
        map((procedimento: HttpResponse<Procedimento>) => procedimento.body)
      );
    }
    return of(new Procedimento());
  }
}

export const procedimentoRoute: Routes = [
  {
    path: '',
    component: ProcedimentoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Procedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ProcedimentoDetailComponent,
    resolve: {
      procedimento: ProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Procedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ProcedimentoUpdateComponent,
    resolve: {
      procedimento: ProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Procedimentos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ProcedimentoUpdateComponent,
    resolve: {
      procedimento: ProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Procedimentos'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const procedimentoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ProcedimentoDeletePopupComponent,
    resolve: {
      procedimento: ProcedimentoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Procedimentos'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
