import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IProcedimento, Procedimento } from 'app/shared/model/procedimento.model';
import { ProcedimentoService } from './procedimento.service';
import { IProfessor } from 'app/shared/model/professor.model';
import { ProfessorService } from 'app/entities/professor';
import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { TipoProcedimentoService } from 'app/entities/tipo-procedimento';
import { IAluno } from 'app/shared/model/aluno.model';
import { AlunoService } from 'app/entities/aluno';

@Component({
  selector: 'jhi-procedimento-update',
  templateUrl: './procedimento-update.component.html'
})
export class ProcedimentoUpdateComponent implements OnInit {
  isSaving: boolean;

  professors: IProfessor[];

  tipoprocedimentos: ITipoProcedimento[];

  alunos: IAluno[];

  editForm = this.fb.group({
    id: [],
    data: [null, [Validators.required]],
    descricao: [],
    professor: [],
    tipo: [],
    aluno: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected procedimentoService: ProcedimentoService,
    protected professorService: ProfessorService,
    protected tipoProcedimentoService: TipoProcedimentoService,
    protected alunoService: AlunoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ procedimento }) => {
      this.updateForm(procedimento);
    });
    this.professorService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfessor[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfessor[]>) => response.body)
      )
      .subscribe((res: IProfessor[]) => (this.professors = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.tipoProcedimentoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITipoProcedimento[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITipoProcedimento[]>) => response.body)
      )
      .subscribe((res: ITipoProcedimento[]) => (this.tipoprocedimentos = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.alunoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IAluno[]>) => mayBeOk.ok),
        map((response: HttpResponse<IAluno[]>) => response.body)
      )
      .subscribe((res: IAluno[]) => (this.alunos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(procedimento: IProcedimento) {
    this.editForm.patchValue({
      id: procedimento.id,
      data: procedimento.data != null ? procedimento.data.format(DATE_TIME_FORMAT) : null,
      descricao: procedimento.descricao,
      professor: procedimento.professor,
      tipo: procedimento.tipo,
      aluno: procedimento.aluno
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const procedimento = this.createFromForm();
    if (procedimento.id !== undefined) {
      this.subscribeToSaveResponse(this.procedimentoService.update(procedimento));
    } else {
      this.subscribeToSaveResponse(this.procedimentoService.create(procedimento));
    }
  }

  private createFromForm(): IProcedimento {
    return {
      ...new Procedimento(),
      id: this.editForm.get(['id']).value,
      data: this.editForm.get(['data']).value != null ? moment(this.editForm.get(['data']).value, DATE_TIME_FORMAT) : undefined,
      descricao: this.editForm.get(['descricao']).value,
      professor: this.editForm.get(['professor']).value,
      tipo: this.editForm.get(['tipo']).value,
      aluno: this.editForm.get(['aluno']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProcedimento>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfessorById(index: number, item: IProfessor) {
    return item.id;
  }

  trackTipoProcedimentoById(index: number, item: ITipoProcedimento) {
    return item.id;
  }

  trackAlunoById(index: number, item: IAluno) {
    return item.id;
  }
}
