export * from './procedimento.service';
export * from './procedimento-update.component';
export * from './procedimento-delete-dialog.component';
export * from './procedimento-detail.component';
export * from './procedimento.component';
export * from './procedimento.route';
