import { NgModule } from '@angular/core';

import { SaapAppSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [SaapAppSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [SaapAppSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SaapAppSharedCommonModule {}
