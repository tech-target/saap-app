import { IProcedimento } from 'app/shared/model/procedimento.model';

export interface IAluno {
  id?: number;
  matricula?: string;
  procedimentos?: IProcedimento[];
}

export class Aluno implements IAluno {
  constructor(public id?: number, public matricula?: string, public procedimentos?: IProcedimento[]) {}
}
