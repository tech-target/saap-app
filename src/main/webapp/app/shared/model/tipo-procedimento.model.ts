export interface ITipoProcedimento {
  id?: number;
  nome?: string;
  descricao?: string;
}

export class TipoProcedimento implements ITipoProcedimento {
  constructor(public id?: number, public nome?: string, public descricao?: string) {}
}
