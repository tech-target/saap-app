export interface IProfessor {
  id?: number;
}

export class Professor implements IProfessor {
  constructor(public id?: number) {}
}
