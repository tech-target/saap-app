import { Moment } from 'moment';
import { IProfessor } from 'app/shared/model/professor.model';
import { ITipoProcedimento } from 'app/shared/model/tipo-procedimento.model';
import { IAluno } from 'app/shared/model/aluno.model';

export interface IProcedimento {
  id?: number;
  data?: Moment;
  descricao?: string;
  professor?: IProfessor;
  tipo?: ITipoProcedimento;
  aluno?: IAluno;
}

export class Procedimento implements IProcedimento {
  constructor(
    public id?: number,
    public data?: Moment,
    public descricao?: string,
    public professor?: IProfessor,
    public tipo?: ITipoProcedimento,
    public aluno?: IAluno
  ) {}
}
