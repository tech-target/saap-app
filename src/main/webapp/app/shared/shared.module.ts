import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SaapAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [SaapAppSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [SaapAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SaapAppSharedModule {
  static forRoot() {
    return {
      ngModule: SaapAppSharedModule
    };
  }
}
