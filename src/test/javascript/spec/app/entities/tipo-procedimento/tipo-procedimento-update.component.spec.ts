/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { SaapAppTestModule } from '../../../test.module';
import { TipoProcedimentoUpdateComponent } from 'app/entities/tipo-procedimento/tipo-procedimento-update.component';
import { TipoProcedimentoService } from 'app/entities/tipo-procedimento/tipo-procedimento.service';
import { TipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

describe('Component Tests', () => {
  describe('TipoProcedimento Management Update Component', () => {
    let comp: TipoProcedimentoUpdateComponent;
    let fixture: ComponentFixture<TipoProcedimentoUpdateComponent>;
    let service: TipoProcedimentoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SaapAppTestModule],
        declarations: [TipoProcedimentoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TipoProcedimentoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TipoProcedimentoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TipoProcedimentoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TipoProcedimento(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TipoProcedimento();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
