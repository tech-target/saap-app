/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SaapAppTestModule } from '../../../test.module';
import { TipoProcedimentoDeleteDialogComponent } from 'app/entities/tipo-procedimento/tipo-procedimento-delete-dialog.component';
import { TipoProcedimentoService } from 'app/entities/tipo-procedimento/tipo-procedimento.service';

describe('Component Tests', () => {
  describe('TipoProcedimento Management Delete Component', () => {
    let comp: TipoProcedimentoDeleteDialogComponent;
    let fixture: ComponentFixture<TipoProcedimentoDeleteDialogComponent>;
    let service: TipoProcedimentoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SaapAppTestModule],
        declarations: [TipoProcedimentoDeleteDialogComponent]
      })
        .overrideTemplate(TipoProcedimentoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TipoProcedimentoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TipoProcedimentoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
