/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SaapAppTestModule } from '../../../test.module';
import { TipoProcedimentoDetailComponent } from 'app/entities/tipo-procedimento/tipo-procedimento-detail.component';
import { TipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

describe('Component Tests', () => {
  describe('TipoProcedimento Management Detail Component', () => {
    let comp: TipoProcedimentoDetailComponent;
    let fixture: ComponentFixture<TipoProcedimentoDetailComponent>;
    const route = ({ data: of({ tipoProcedimento: new TipoProcedimento(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SaapAppTestModule],
        declarations: [TipoProcedimentoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TipoProcedimentoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TipoProcedimentoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tipoProcedimento).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
