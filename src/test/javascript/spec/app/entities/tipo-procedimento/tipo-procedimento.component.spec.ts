/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SaapAppTestModule } from '../../../test.module';
import { TipoProcedimentoComponent } from 'app/entities/tipo-procedimento/tipo-procedimento.component';
import { TipoProcedimentoService } from 'app/entities/tipo-procedimento/tipo-procedimento.service';
import { TipoProcedimento } from 'app/shared/model/tipo-procedimento.model';

describe('Component Tests', () => {
  describe('TipoProcedimento Management Component', () => {
    let comp: TipoProcedimentoComponent;
    let fixture: ComponentFixture<TipoProcedimentoComponent>;
    let service: TipoProcedimentoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SaapAppTestModule],
        declarations: [TipoProcedimentoComponent],
        providers: []
      })
        .overrideTemplate(TipoProcedimentoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TipoProcedimentoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TipoProcedimentoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TipoProcedimento(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.tipoProcedimentos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
